# Tržiště
## Klíč

`Am H7 Em`

1. Kupci `Em`prodali `D`poslední `Em`zboží,
zamkli `G`krámy a `D`odešli `G`pryč,
dav `Am`naslouchal kázání `Em`z lóží
o tom, `C`jak chutná cukr a `H7`bič.

2. Každý stál a tiše se díval
na ten vyprodej hanby a lží,
někdo nadával a jiný zíval,
kdekdo `C`za rohem myslel si `D`svý.

>`Am Em C D G Am Em C D H7`(instr.)

3. Stačí písknout, a poslušně skáčem,
jenom čekáme, co bude dál,
když nám nadrobí, radostí pláčem,
žijem život jak maškarní bál.

4. Stále umíme ohýbat záda,
klečet před těmi, co všechno smí,
člověk s člověkem o trůn se hádá,
láska s pravdou jen pod mostem spí.

`Em D C H7`

> (instr.)

5. Kupci prodali poslední zboží
v tomhle tržišti slibů a lží,
pak vstali a odešli z lóží,
z tváří `C`lidí se `H7`vytratil `Em`smích.
