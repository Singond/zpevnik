# Proměny
## Čechomor

1. `Am`Darmo sa ty trápíš, `G`můj milý `C`synečku,
nenosím já tebe, `E7`nenosím `Am`v srdéčku.
Přece tvo`G C G C`ja nebudu, `Dm`ani jednu `E7`hodi`Am`nu.

1. Copak sobě myslíš, má milá panenko,
vždyť ty jsi to moje rozmilé srdénko.
A ty musíš býti má, lebo mi tě pán Bůh dá.

1. A já sa udělám malú veveričkú,
a uskočím tobě z dubu na jedličku.
Přece tvoja nebudu, ani jednu hodinu.

1. A já chovám doma takú sekerečku,
ona mi podetne důbek i jedličku.
A ty musíš býti má, lebo mi tě pán Bůh dá.

1. A já sa udělám tu malú rybičkú,
a já ti uplynu pryč po Dunajíčku.
Přece tvoja nebudu, ani jednu hodinu.

1. A já chovám doma takovú udičku,
co na ni ulovím kdejakú rybičku.
A ty přece budeš má, lebo mi tě pán Bůh dá.

`Am F C F C G`

7. A já sa udělám tú velikú vranú
a já ti uletím na uherskú stranu.
Přece tvoja nebudu, ani jednu hodinu.

1. A já chovám doma starodávnú kušu,
co ona vystřelí všeckým vranám dušu.
A ty musíš býti má, lebo mi tě pán Bůh dá.

1. A já sa udělám hvězdičkú na nebi
a já budu lidem svítiti na zemi.
Přece tvoja nebudu, ani jednu hodinu.

1. A sú u nás doma takoví hvězdáři,
co vypočítajú hvězdičky na nebi.
A ty mus íš býti má, lebo mi tě pán Bůh dá.

1. A ty musíš býti má, lebo mi tě pán Bůh dá.

`Am F C F C G`
