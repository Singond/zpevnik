# Víno
## Klíč

> `Em`Víno teď `D`nalej nám, `Em`ať si každej, `C`co chce `H7`zpívá,
`Em`vína `D`plnej džbán, dnes `Em`do rána `H7`budeme `Em`pít.

>> Víno nalej nám, po boji jen žízeň zbývá,
zítra někdo z nás třeba už nebude žít.

1. `Em`Než přijde `D`den, každý `C`má `H7`času `Em`dost,
zbývá jen `D`zapít žal, `G`klít `D`a mít `G`zlost,
že zas je nás `D`čím dál míň, `Em`víra je tu jen `H7`host,
`Em`proč se `D`máme bít `C`druhejm `H7`pro ra`Em`dost.

> Víno teď nalej nám…

2. Krátí se noc, v krčmě všem slábne hlas,
ráno zavelí a kdekdo zláme vaz,
[: už zní rozkaz, dál bít se můžem, třeba i krást,
ten, kdo přežije, tomu zbyde chlast. :]

> Víno teď nalej nám…

>> Víno nalej nám…

> `F#m`Víno teď `E`nalej nám, `F#m`ať si každej, `D`co chce `C#7`zpívá,
`F#m`vína `E`plnej džbán, dnes `F#m`do rána `C#7`budeme `F#m`pít.

>> Víno nalej nám…

### Cd.
`A`Zpívej až `E`do rána, `F#m`není každá bitva `D`prohra`C#7`ná,
`F#m`dojde i `E`na pána, `F#m`a pak si `C#7`budeme `F#m`žít.
Na na na…
