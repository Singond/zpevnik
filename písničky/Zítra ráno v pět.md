# Zítra ráno v pět
## Jarek Nohavica

1. Až mě `Am`zítra ráno v pět `C`ke zdi postaví
ješ`F`tě si napos`G`led dám `C`vodku na zdra`A`ví
z očí `Dm`pásku strhnu `G`si to abych `C`viděl na ne`Am`be
a `Dm`pak vzpomenu `E7`si `Am`lásko na te`Am7`be
`Dm`ná-na ná-na `G`ná na `C`ná-na ná-na `Am`ná
a `Dm`pak vzpomenu `E7`si na te`Am`be.

2. Až zítra ráno v pět přijde ke mně kněz
řeknu mu že se splet že mně se nechce do nebes
že žil jsem jak jsem žil a stejně tak i dožiju
a co jsem si nadrobil to si i vypiju
ná-na ná-na ná na ná-na ná-na ná
a co jsem si nadrobil si i vypiju.

3. Až zítra ráno v pět poručík řekne pal
škoda bude těch let kdy jsem tě nelíbal
ještě slunci zamávám a potom líto přijde mi
že tě lásko nechávám samotnou tady na zemi
ná-na ná-na ná na ná-na ná-na ná
že tě lásko nechávám na zemi.

4. Až zítra ráno v pět prádlo půjdeš prát
a seno obracet já u zdi budu stát
tak přilož na oheň a smutek v sobě skryj
prosím nezapomeň nezapomeň a žij
ná-na ná-na ná na ná-na ná-na ná
na mě nezapomeň a žij.
