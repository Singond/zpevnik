# Hej, bystrá voda
## Čechomor

1. [: `D`Hej, bystrá voda, bystrá vodička, `G`plakalo děvče `A`pro Janíčka. :]
[: `A`Hej, lese temný, `D`vršku zelený, `G`kde je můj Janík `A`přemile`D`ný? :]

2. [: Hej, povídali, hej, povídali, hej, že Janíčka pobodali. :]
[: Hej, pobodali ho Oravjani, hej, za ovečky za berany. :]

3. [: Hej, bystrá voda, bystrá vodička, plakalo děvče pro Janíčka. :]
[: Hej, u té zadní oravské stěny, leží Janíček zahlušený. :]

4. Hej, bystrá voda, bystrá vodička, plakalo děvče pro Janíčka.
