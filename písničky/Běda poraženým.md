
# Běda poraženým
## Klíč


1. `C`Běží krajem `Em`dlouhá cesta, `F`do svatého `G`města tě zavede,
`C`podél cesty `Em`kříže s těly, `F`s těly těch, co `G`neuspěli,
`F Am`vae vic`G`tis, `C G`amen, `F G C`amen.

1. Tisíce se zvedly v právu, neměly nic a slávu si dobyly,
ponížený zvedá hlavu, veden vírou vlastní síly,
vae victis, amen, amen.

1. Plamen vzpoury letí státem, otroci pouť volnou si zvolili,
otrokář však platí zlatem, římská vojska valem sílí,
vae victis, amen, amen.

1. Marné bylo odhodlání, jiskra žití sklání se před silou,
umírali za svítání, pod nohama cestu bílou,
vae victis, amen, amen.

1. Běží krajem dlouhá cesta, do svatého města tě zavede,
podél cesty kříže s těly, s těly těch, co neuspěli,
vae victis, amen, amen.
