# Trubadůrská
## Karel Plíhal

1. `Am`Od hradu `Em`ke hradu `Am`putujem, `C`zpíváme `G`a holky `E7`muchlujem.
[: `Am`Dřív ji``Am/G ``nam `Fmaj7` nejed``Fmaj7/F# ``em, `Am`dokud tu `Em`poslední `Am`nesvedem. :]

2. Kytary nikdy nám neladí, naše písně spíš kopnou než pohladí,
[: nakopnou zadnice ctihodných měšťanů z radnice. :]

> `G`Hop hej, je `C`ve``Em7 Am Am/G ``selo, pan `Fmaj7 `kníže ``Fmaj7/F# ``pozval `G`kejklíře,
`G`hop hej, je `C`ve``Em7 Am Am/G ``selo, dnes `Fmaj7 `vítaní `Em`jsme `Am`hosti.
Hop hej, je veselo, ač nedali nám talíře,
hop hej, je veselo, pod stůl nám hážou kosti.

2. Nemáme způsoby knížecí, nikdy jsme nejedli telecí,
[: spáváme na seně, proto vidíme život tak zkresleně. :]

4. A doufáme, že lidi pochopí, že pletou si na sebe konopí,
[: že hnijou zaživa, když brečí v hospodě u piva. :]

> Hop hej…

5. Ale jako bys lil vodu přes cedník, je z tebe nakonec mučedník,
[: čekaj' tě ovace a potom veřejná kremace. :]

5. Rozdělaj' pod náma ohýnky a jsou z toho lidové dožínky.
Kdo to je tam u kůlu, ale příliš si otvíral papulu.
Kdo to je tam u kůlu, borec, za nás si otvíral papulu.

> Hop hej…

7. Od hradu ke hradu putujem, zpíváme a holky muchlujem.
[: Dřív jinam nejedem, dokud tu poslední nesvedem. :]

8. To radši zaživa do hrobu, než pověsit kytaru na skobu
a v hospodě znuděně… čekat…
