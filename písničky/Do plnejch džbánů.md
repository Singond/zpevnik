# Do plnejch džbánů
## Ginevra

> Tak `D`do plnejch `A`džbánů, Bět`D`ko, nalej `G`nám `D`práce `A`už máme `D`dost,
`D`hospoda `A`na rynku `D`to je náš `G`chrám, `D`zaženem `A`bídu a `D`zlost,
`Em`že nejsme bohatí, `F#m`to je nám fuk, `G`lepší se v chýši `A`smát.
Tak `D`do plnejch `A`džbánů, Bět`D`ko, nalej `G`nám, `D`grošů je `A`tak ako`D`rát.

1. `D`Nač pytel `G`s penězi `D`mít,
všude ho `G`tahat a `A`klít,
`Em`bosí ve vodě `F#m`stát,
`G`nad hlavou most a ne `A`hrad.

1. `D`pod širou `G`oblohou `D`spát, ![pivo](ilustrace/pivo.jpg "right")
svý touhy si `G`pod hlavu `A`dát,
`Em`život zas není tak `F#m`zlej,
`G`přidej se bude ti `A G F#m Em`hej. Hej! Hej!

> Tak do plnejch džbánů…

1. Přisedni kdo srdce máš,
jedno co do placu dáš.
Dneska ty zítra zas já,
vždyť každý z nás úděl svůj má.

> Tak do plnejch džbánů…
