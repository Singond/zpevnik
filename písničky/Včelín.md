# Včelín
## Čechomor

1. [: `Am`Sousedovic Věra `G`má `Am`jako žádná `G`jiná,
`Am`viděl jsem ji včera `G`máchat `Am`dole `Em`u vče`Am`lína. :]

> [: `Am`Dole dole dole dole, `C`dole dole dole,
`G`hej, dole dole dole, dole u vče`Am`lína. :]

2. [: Líčka jako růže máš, já tě musím dostat,
nic ti nepomůže spát - skočím třeba do sna. :]

> Dole dole…

3. [: Ať v poledne radost má, slunko hezky hřeje,
když se na mě podívá, dám jí co si přeje. :]

> Dole dole…

4. [: Líčka jako růže máš, zajdu k panu králi,
ať přikázat vašim dá, aby mi tě dali. :]

> Dole dole…
