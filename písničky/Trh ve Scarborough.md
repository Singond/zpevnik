# Trh ve Scarborough
## Spirituál kvintet

1. `Am`Příteli, máš do `G`Scarborough `Am `jít,
`C`dobře `Am`vím, že `C`půj`D`deš tam `Am`rád,
tam dívku `C`najdi na ``G``Mar``Am``ket `G`Street,
`Am`co chtěla `D`dřív ``F``mou `G`ženou se `Am`stát.

2. Vzkaž ji, ať šátek začne mi šít,
za jehlu rýč však smí jenom brát
a místo příze měsíční svit,
bude-li chtít mou ženou se stát.

3. Až přijde máj a zavoní zem,
šátek v písku přikaž ji prát
a ždímat v kvítku jabloňovém,
bude-li chtít mou ženou se stát.

4. Z vrkočů svých ať uplete člun,
v něm se může na cestu dát,
s tím šátkem pak ať vejde v můj dům,
bude-li chtít mou ženou se stát.

5. Kde útes ční nad přívaly vln,
zorej dva sáhy pro růží sad,
za pluh ať slouží šípkový trn,
budeš-li chtít mým mužem se stát.

6. Osej ten sad a slzou jej skrop,
choď těm růžím na loutnu hrát,
až začnou kvést, tak srpu se chop,
budeš-li chtít mým mužem se stát.

7. Z trní si lůžko zhotovit dej,
druhé z růží pro mě nech stát,
jen pýchy své a boha se ptej,
proč nechci víc tvou ženou se stát.
