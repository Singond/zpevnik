# Nářek
## Klíč

1. `H7`Nemám myslet `Em`na chlapy, `D`nemám žebrat `G`o lásku,
`Am`věčně snášet `Em`útrapy, `H7`co z života `Em`mám?

2. Nemám pít a nemám klít, nemám vlastní děti bít,
nemám nic a nemám klid, tak co teda mám?

3. Nemám šlapat průchody, nemám zdrhat z hospody,
nepřekážet pod schody, prej se ztratit mám.

4. Ani chlapy, ani chlast, ani žebrat, ani krást,
ani Boha, ani vlast, tak co teda mám?

5. Na na na…
