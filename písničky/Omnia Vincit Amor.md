# Omnia Vincit Amor
## Klíč

1. `Am`Šel pocestný kol `G`hospodských `Am`zdí, `C`  přisedl k nám a `G`lokálem `C`zní
`Dm`pozdrav jak svaté `Am`přikázá`G`ní: `Am`Omnia `G`vincit `Am`amor.

2. Hej, šenkýři, dej plný džbán, ať chasa ví, kdo k stolu je zván,
se mnou ať zpívá, kdo za své vzal: Omnia vincit Amor.

> Zlaťák `C`pálí, `G`nesleví `Am`nic, štěstí `C`v lásce `G`znamená `C`víc,
všechny `Dm`pány `C`ať vezme `G`ďas!`E`
`Am`Omnia `G`vincit `Am`amor.

3. Já viděl zemi válkou se chvět, musel se bít a nenávidět,
v plamenech pálit prosby a pláč -- Omnia vincit amor.

4. Zlý trubky troubí, vítězí zášť, nad lidskou láskou roztáhli plášť,
vtom kdosi krví napsal ten vzkaz: Omnia vincit amor.

> Zlaťák pálí…

5. Já prošel každou z nejdelších cest, všude se ptal, co značí ta zvěst,
až řekl moudrý: „Pochopíš sám, všechno přemáhá láska.“

> Zlaťák pálí…

6. Teď s novou vírou obcházím svět, má hlava zšedla pod tíhou let,
každého zdravím tou větou všech vět: Omnia vincit amor.

Omnia vincit amor.
