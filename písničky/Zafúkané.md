# Zafúkané
## Fleret

1. `Am`Větr sněh `Asus2`zanésl `Am`z hor do po`Asus2`lí,
`Am`já idu `C`přes kopce, `G`přes údo`Am`lí.
`C`Idu k tvéj `G`dědině zatúla`C`nej,
`F`cestičky `C`sněhem sú `E`zafúka`Am`né.
`Fmaj7 Am Esus4`…

> `Am`Zafúka`C`né, `G`zafúka`C`né, `F`kolem mňa `C`všecko je `Dm`zafúka`E`né.
`Am`Zafúka`C`né, `G`zafúka`C`né, `F`kolem mňa `Dm`všecko je `E`zafúka`Am`né.
`Em D G H7`…

2. Už vašu chalupu z dálky vidím,
srdce sa ozvalo, bit ho slyším.
Snáď enom pár kroků mi zostává
a budu u tvého okénka stát.

> [: Ale zafúkané, zafúkané, okénko k tobě je zafúkané. :]

3. Od tvého okna sa smutný vracám,
v závějoch zpátky dom cestu hledám.
Spadl sněh na srdce zatúlané
aj na mé stopy sú zafúkané.

> [: Zafúkané, zafúkané, mé stopy k tobě sú zafúkané. :]
