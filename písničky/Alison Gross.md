# Alison Gross
## Asonance

1. Když `Em`zapadlo `D`slunce a `G`vkradla se `H7`noc
a `Em`v šedivých `D`mracích se `G`ztrá``Am``cel `H7`den
a `Em`když síly `D`zla ve tmě `G`převzaly `H7`moc,
tu `Em`Alison `D`Gross vyšla z hradu `Em`ven.

1. Tiše se vplížila na můj dvůr,
a jak oknem mým na mě pohlédla,
tak jen kývla prstem a já musel jít
a do komnat svých si mě odvedla.

> `Em`Alison Gross a černý `D`hrad,
ze `G`zlověstných `D`skal jeho hradby `Em`ční,
Alison Gross, nejodporněj`D`ší
ze všech `G`čaroděj`H7`nic v zemi sever`A H7`ní.

3. Složila mou hlavu na svůj klín
a sladkého vína mi dala pít.
„Já můžu ti slávu i bohatství dát,
jen kdybys mě chtěl za milenku mít.“

1. „Mlč a zmiz, babo odporná,
slepý jak krtek bych musel být,
to radši bych na špalek hlavu chtěl dát,
než Alison Gross za milenku mít.“

> Alison Gross a černý hrad…

1. Přinesla plášť celý z hedvábí,
zlatem a stříbrem se celý skvěl.
„Kdybys jen chtěl se mým milencem stát,
tak dostal bys vše, co bys jenom chtěl.“

1. Pak přinesla nádherný zlatý džbán
bílými perlami zářící.
„Kdybys jen chtěl se mým milencem stát,
i darů bys měl plnou truhlici.“

1. „Stůj a mlč, babo odporná,
slepý jak krtek bych musel být,
to radši bych na špalek hlavu chtěl dát,
než milencem tvým na chvíli se stát.“

> Alison Gross a černý hrad…

1. Tu k ohyzdným rtům zvedla černý roh
a natřikrát na ten roh troubila
a s každým tím tónem mi ubylo sil,
až všechnu mou sílu mi sebrala.

1. Pak Alison Gross vzala čarovnou hůl
a nad mojí hlavou s ní kroužila
a podivná slova si zamumlala
a v slizkého hada mě zaklela.

> Alison Gross a černý hrad…

1. Tak uplynul rok a uplynul den
a předvečer svátku všech svatých byl
a tehdy na místě, kde žil jsem jak had,
se zjevila královna lesních víl.

1. Dotkla se mě třikrát rukou svou
a její hlas kletbu rozrazil,
a tak mi zas vrátila podobu mou,
že už jsem se dál v prachu neplazil.

> Alison Gross a černý hrad…
