# Javor
## Nerez

1. `Em F#dim G C Am Em H7 Em` Laj, laj, laj…

1. `Em`Tak už to, `F#dim`miláčku, `G`musíme `C`brát
`Am`chtěl bych tě `D7`naposled `H7`pomilo`Em`vat
tak už to, `F#dim`miláčku, `G`u nás cho`C`dí
`Am`javor své `D7`listy hned `H7`dvakrát sho`C`dí
`Am`zbývá nám `D7`balada `H7`o nadě`Em`ji
`F#dim`zejtra mě `Em`na vojnu `Edim`odvádě`H7`jí. ![javor](ilustrace/javor.jpg "right")

2. Tak už to, miláčku, musíme brát
chtěl jsem ti svou lásku naslibovat
javor však své listy dvakrát shodí
jinej tě k oltáři doprovodí
zbývá mi balada o naději
zejtra mě na vojnu odvádějí.

3. Teď jsem si, miláčku, jen jedním jist
javor, ten shodí i poslední list
i kdyby ten javor v bouři blesk sťal
nikdy bych svou lásku nesliboval
zpívám si baladu o naději
zejtra mě na vojnu odvádějí.

3. `Em F#dim G Am F#dim Em Edim H7 Em F#dim G Am Edim H7 Em`Laj laj laj…
