# Zelené pláně
## Spirituál kvintet

1. `Am`Tam, kde zem `Dm`duní `Am`kopyty `E`stád,
`Am`znám plno `Dm`vůní, co `C`dejchám je tak `G`rád,
`F`čpí tam pot `G`koní a `C`voní tymi`A7`án,
`Dm`kouř obzor `G`cloní, jak `C`dolinou je `E`hnán,
`Am`rád žiju `Dm`na ní, tý `Am E`pláni `Am`zelený.

1. Tam, kde mlejn s pilou proud řeky hnal,
já měl svou milou a moc jsem o ni stál,
až přišlo psaní, ať na ni nečekám,
prý k čemu lhaní, a tak jsem zůstal sám,
sám znenadání v tý pláni zelený.

### B.
`F`Dál čistím chlív a `G`lovím v oře`C`ší,
`F`jenom jako dřív mě `G`žití netě`C`ší,
`Am`když hlídám stáj a `Dm`slyším vítr dout,
`Am`prosím, ať jí `Dm`poví, `Dm/H`že mám v srdci `E`troud.

1. Kdo ví, až se doví z větrnejch stran,
dál že jen pro ni tu voní tymián,
vlak hned ten ranní ji u nás vyloží
a ona k spaní se šťastná uloží
sem, do mejch dlaní, v tý pláni zelený.
