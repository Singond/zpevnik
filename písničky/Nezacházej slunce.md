# Nezacházej, slunce
## Žalman

1. `C`Nezacházej, slunce, neza`Dm`cházej ještě,
`G`já mám potěšení `F`na dale`G`kej cestě,
`C`já mám potě``Em``šen`Am `í `C`na da``Dm``lekej `G`ces`C`tě.

2. Já mám potěšení mezi hory-doly,
[: žádnej neuvěří, co je mezi námi. :]

3. Mezi náma dvouma láska nejstálejší,
[: a ta musí trvat do smrti nejdelší. :]

4. Trvej, lásko, trvej, nepřestávej trvat,
[: až budou skřivánci o půlnoci zpívat. :]
