# Podzimní
## Spirituál kvintet

1. Už `Dm`léto končí, `C`stříbro `A`jí ``D5/F# ``ní `G`sví``D5 Asus4 ``tí `A Dm`v mlází.
Můj `Dm`prázdný dům bůh `C`lásky `A`stá``D5/F# ``le `G`ne``D5 Asus4 ``na `A Dm`chází.
[: `F6|Dm `Vábí `C`mě `F6|Dm`chladná kapka ro`C`sy,
`Dm`vím, `Em `že `D`v sobě `Em`skrývá `C`cosi.
`Dm`Touhu `Hm`mou však `C`nikdy `A`a``D5/F# ``si `G`n``D5 Asus4 ``eu `A Dm`hasí. :]

2. I bílá bříza v údolí už listí ztrácí.
A zpátky na jih v hejnech míří tažní ptáci.
[: Cár mlhy, který se tu válí,
přikryl rudé stráně v dáli.
Skrýt můj stesk mé tváře bledé nedovede. :]
