# Petěrburg
## Jarek Nohavica

1. `Am`Když se snáší noc na střechy Petěrburgu
`F`padá `E`na mě `Am`žal
`Am`zatoulaný pes nevzal si ani kůrku
`F`chleba kterou `E`jsem mu `Am`dal.

> [:``G`` `C`Lásku moji `Dm`kníže I`E`gor si bere
`F`nad sklenkou `Adim`vodky `H7`hraju si `E`s revolverem
`Am`havran usedá na střechy Petěrburgu
`F`čert a`E`by to `Am`spral. :]

2. Nad obzorem letí ptáci slepí
v záři červánků
moje duše široširá stepi
máš na kahánku.

> [: Mému žalu na světě není rovno
vy jste tím vinna Naděždo Ivanovno
vy jste tím vinna až mě zítra najdou
s dírou ve spánku. :]
