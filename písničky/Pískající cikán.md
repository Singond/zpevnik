# Pískající cikán
## Spirituál kvintet

1. `D`Dívka `Em`loudá `F#m`se vini`Em`cí, `D`tam, kde `Em`zídka je `F#m`níz`Em`ká,
`D`tam, kde `Em`stráň končí `F#m`voníc`G`í, tam `D`písnič`G`ku někdo `D`pí`G`s`A7`ká.

> (Píská se na melodii sloky.)

2. Ohlédne se a, propána, v stínu, kde stojí líska,
švarného vidí cikána, jak leží, písničku píská.

3. Chvíli tam stojí potichu, písnička si ji získá,
domů jdou spolu do smíchu, je slyšet, cikán jak píská.

> …

4. Jenže tatík, jak vidí cikána, pěstí do stolu tříská:
„Ať táhne pryč, vesta odraná, groš nemá, něco ti spíská!“

5. Teď smutnou dceru má u vrátek, jen Bůh ví, jak se jí stýská.
„Kéž vrátí se mi zas nazpátek ten, který v dálce si píská.“

> …

6. Pár šídel honí se na louce, v trávě rosa se blýská,
cikán, rozmarýn v klobouce, jde dál a písničku píská.

7. Na závěr zbývá už jenom říct, v čem je ten kousek štístka,
peníze často nejsou nic, 3x[: má víc, kdo po svém :] si píská.

> …
![vazka](ilustrace/vazka.jpg "right")
