# Třináctka
## Klíč

1. Zve `Em`válčit mě pan `D`maršál sám,
`C`v mládí je má `H7`síla, milá,
`Em`víš, jak rád tě `D`mám, a až se vrátím,
`C`třináct růží `H7`na polštář ti `Em`dám.

> [: Už mi to i `G`táta říkal:
`D`koukej žít, `Em`v neštěstí i `Hm`štěstí mít, ![tarot-13](ilustrace/tarot-13.jpg "right")
buď `C`chlap, a neznej `G`klid,
a tarokovou `Am`třináctku,
`H7`tu musíš v kapse `Em`mít. :]

2. Já dávno o své slávě sním,
vínem vlast mě křtila, milá,
Turka porazím a až se vrátím,
zlatek třináct na stůl vysázím. 

> Už mi to i táta říkal…

3. Už v šenku jsem se prával rád,
však jsi při tom byla, milá,
nechtěj osud znát, když nevrátím se,
dej mi z věže zvonit třináctkrát.

> Už mi to i táta říkal…
