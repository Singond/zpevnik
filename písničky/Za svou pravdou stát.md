# Za svou pravdou stát
## Spirituál kvintet

1. Máš `Am`všechny trumfy mládí a `G`ruce čistý `Am`máš,
jen `Dm`na tobě teď `Am`záleží, na `E`jakou hru se `Am`dáš.
[: Musíš `Am`za svou pravdou `E`stát,
    musíš za svou pravdou `Am`stát. :]

2. Už víš, kolik co stojí, už víš, co bys' rád měl,
už ocenil jsi kompromis a párkrát zapomněl.
[: Že máš za svou pravdou stát,
    že máš za svou pravdou stát. :]

3. Už nejsi žádný elév, co prvně do hry vpad,
už víš, jak s králem ustoupit a jak s ním dávat mat.
[: Takhle za svou pravdou stát,
    takhle za svou pravdou stát. :]

4. Teď přichází tvá chvíle, teď nahrává ti čas,
tvůj sok poslušně neuhnul - a ty mu zlámeš vaz!
[: Neměl za svou pravdou stát,
    neměl za svou pravdou stát. :]

5. Tvůj potomek ctí tátu, ty vštěpuješ mu rád
to heslo, které dobře znáš z dob, kdy jsi býval mlád.
   
6. Máš všechny trumfy mládí a ruce čistý máš,
jen na tobě teď záleží, na jakou hru se dáš.
[: Musíš za svou pravdou stát,
    musíš za svou pravdou stát. :]
