# Uherská zem
## Mezitím

1. `Emi`V tej uherskej zemi, kvítek utrže`D`ný,
`C`už jsem moje mi`Em`lá, už jsem odvedený `C D`jéé
`C`už jsem odvede`Em`ný,
`C`už jsem `D` odvede`Em`ný.

2. Šavličku broušenou, klisničku kovanou,
Turka jedeš bíti, pojedu já s tebou jéé [: pojedu já s tebou. :]

3. Na turecký straně k boji už se chystá,
milý koně sedlá, bitva už je jistá jéé [: bitva už je jistá. :] 
   
>`C`Šiky se `G`řadí `C`v mohutný `D`val
`Em`a poklus `C`koní mění se v `D`cval,
`C`nad hlavou `G`praporec `C`zaplápo`D`lal,
`Em`s tasenou `C`šavlí `D`jedem se `Em`bít, `D`za krá`Em`le.

4. Zbrojí pozlacenou, šavlí zakřivenou, milý z koně padá
s hlavou zkrvavenou jéé [: s hlavou zkrvavenou. :]

5. Uši jí uřezali, oči jí vybodali
a tu mojí milou Turci sebou vzali jéé [: Turci ji sebou vzali. :]

6. V poli bílý kosti černý vrány hostí,
sbohem ty můj milý, sbohem na věčnosti jéé [: sbohem na věčnosti. :] 