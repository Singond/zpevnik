# Půlnoční rej
## Ginevra

1. `Am`Až na věži odbije půlnoc, `G`hopsa, `C`hejsa,
víka rakví `Am`odkryjem, rejdů `Em`nastal `Am`čas.
Rok se v hrobě válíme a tlíme, `G`hopsa, `C`hejsa, ![kostlivec](ilustrace/kostlivec.jpg "right")
do kola, hop! se `Am`těšíme, nocí `Em`volá `Am`hlas.

> Pojď už `C`ven, `G`skočnou zatan`Am G`číme.
`Am`Přijď už `C`sem, `Em`jak se, bratře, `Am`máš?
Polez `C`z hlíny, hej! `G`Kostmi zachras`Am G`tíme.
`Am`Než se `C`vrátí den, `Em`noc je život `Am`náš!

2. Podej víno, nalejem číše, hopsa, hejsa,
smrti do dna připijem na hříchy a špás.
Hlínou bradu přikrejem k ránu, hopsa, hejsa,
Za hřbitovním kostelem spočinout je čas.

> Pojď už ven…

1. Za rok z díry vylezem do tmy, hopsa, hejsa,
hnáty v tanci protřesem, Měsíc bude hřát.
Ať jsi hříšník nebo sám anděl, hopsa, hejsa
Voják, zloděj, nebo pán, pod zem půjdeš spát.

> Pojď už ven…
