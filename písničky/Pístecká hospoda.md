# Pístecká hospoda

1. `Ami`Pístecká hospoda `G`z drobného `E`kamení,
`Ami`kdo kdy do ní `G`vej`E`de `Ami`pro ob`E`ve`Ami`selení.

2. Přišlo pro mě psaní, že na vojnu musím,
do cizí krajiny odejíti musím.

3. Dali na mě kabát ze sukna bílého,
že se nemusím bát do pole širého.

4. Ach má nejmilejší, od tebe mám šátek,
ten mě vždy potěší, až budu mít smutek.

5. Přikryj mě, má milá, červeným brokátem,
abys mě poznala až budu soldátem.

6. Přikryj mě, má milá, červeným damaškem,
abys mě poznala, až budu pod vrškem.
