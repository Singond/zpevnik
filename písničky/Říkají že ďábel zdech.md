# Říkají, že ďábel zdech
## Asonance

> `C`Říkají, že ďábel zdech', že `F`ďábel zdech', že ďábel zdech',
`C`říkají, že ďábel zdech', teď v pekle tahá `G7`klá`C`dy.
Tvrdím vám a nejsem sám, já tvrdím vám a nejsem sám
že živý vstal a rovnou dal se najmout do armády.

1. Vojákem být -- jaká čest boj ztvrdí rys i sval i pěst.
Kdo k vítězství chce vojsko vést ten dočká se jen slávy.
Za vítězstvím žeň se dál, dbej, abys vždy v čele stál!
Vojna ztvrdí pěst i sval a mozek ztvrdne záhy.

> Říkají…

2. Phil McKrijt byl tvrdej chlap, jo, tvrdej chlap a žádnej srab,
když nepřítele v boji drap' tak byl s ním vždycky ámen.
Při souboji dostal křeč protivník vyrazil v zteč
však při výpadu zlomil meč o játra jako kámen.

> Říkají…

3. Čím chceš být, řekni, synku můj, dnes rozhodni se stůj co stůj,
víc doma planě nelelkuj a nezahálej stále.
„Chytrý nejsem, to vím sám, také k práci odpor mám,
na vojnu se tedy dám a budu generálem!“

> Říkají…
