# Divokej horskej tymián
## Žalman 

1. Dál za `C`vozem `F`spolu šl`C`apem,
někdo `F`rád a někdo `C`zmaten, ![tymian](ilustrace/tymian.jpg "right")
kdo se `F`vrací, není `Em`sám,
je to `Dm`věc, když pro nás `F`voní
z hor di`C`vokej `F`tymi`C`án.

2. Léto, zůstaň dlouho s námi,
dlouho hřej a spal nám rány,
až po okraj naplň džbán,
je to věc, když pro nás voní
z hor divokej tymián.

> Podí`F`vej, jak málo `C`stačí,
když do `F`vázy natr`Em7`hám
bílou `Dm`nocí k milo`F`vání
z hor di`C`vokej `F`tymi`C`án.

3. Dál za vozem trávou, prachem,
někdy krokem, někdy trapem,
kdo se vrací dolů k nám,
je to věc, když pro nás voní
z hor divokej tymián.

> Podívej…
