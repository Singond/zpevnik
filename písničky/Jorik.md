# Jorik
## Mezitím

> `Em`Hej hospodo nalej `D`žoldákům, `Em`vrchovatě vína `D`všem,
ještě `G`dneska pijem, holky `D`milujem a `Em`zejtra`H7` už tu nebu`Em`dem.

1. `Em`Železnou přilbici, `D`na stole v sklenici `Em`víno `H7`a v něm je `Em`psáno,
večer `G`před bitvou patří `D`žoldákům,
vždyť `Em`pán Bůh ví `H7`co bude `Em`ráno.
`G`Ospalí vínem `D`k půlnoci `G`spánek jim na oči `D`sedá,
`G`jen Jorik u krbu `D`popíjí, `Em`svůj osud `H7`v plamenech `Em`hledá.

> Hej hospodo…

1. S půlnocí postava ve dveřích, smáčenej plášť k zemi padá,
pojednou Jorik svejm očím nevěří, stojí tam krásná a mladá.
Dolej si vína a pozvedni číš, přisedni ke krbu blíž,
celou noc Joriku vyprávěj a chceš-li mne líbat, tak smíš.

> Hej hospodo…

1. K ránu ta dívka zas přehodí plášť, Má paní dřív než se ztratíš,
Jorik povídá, ještě mi přísahej, že po bitvě ke mně se vrátíš.
Než trubač vítězství odtroubí, můj milej, ta chvíle se blíží,
dřív než slunce se s večerem zasnoubí,
naše šlépěje opět se zkříží.

> Hej hospodo…

&nbsp;

1. Už dozněly bubny nad bitvou, už zvony hlaholí z věží,
jen mrtvé paní Smrt obchází, míří tam, kde Jorik leží,
odhrne kápi a hovoří, proč chtěl jsi mou přísahu ranní,
včera ses se Smrtí miloval, dnes bude Smrt tvojí paní.

> Hej hospodo…
