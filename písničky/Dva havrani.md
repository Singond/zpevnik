# Dva havrani
## Asonance

1. Když jsem se `Dm`z pole `C`vrace`Dm`la,
dva havrany jsem `C`slyše`Dm`la,
jak jeden `F`druhého se ptá`C`:
[: „`Dm`Kdo dneska veče`C`ři nám `Dm`dá?“ :]

1. Ten první k druhému se otočil
a černým křídlem cestu naznačil,
krhavým zrakem k lesu hleděl
[: a takto jemu odpověděl :]

1. „Za starým náspem, v trávě schoulený ![havran](ilustrace/havran.jpg "right")
tam leží rytíř v boji raněný,
a nikdo neví, že umírá,
[: jen jeho kůň a jeho milá.“ :]

1. „Jeho kůň dávno po lesích běhá
a jeho milá už jiného má,
už pro nás bude dosti místa,
[: hostina naše už se chystá.“ :]

1. „Na jeho bílé tváře usednem
a jeho modré oči vyklovem,
a až se masa nasytíme,
[: z vlasů si hnízdo postavíme.“ :]
