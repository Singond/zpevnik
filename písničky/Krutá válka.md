# Krutá válka
## Spirituál kvintet

`C Am7 Dm E C F G F6 C`

1. Tmou `C`zní zvony z `Am`dálky, o `Dm`čem to, milý, ``Em``sníš,
`E`hoří `F`dál plamen `Dm`války a `C`rá``F6``no je `G G7`blíž,
chci `C`být stále `Am`s tebou, až `Dm`trubka začne ``Em``znít,
`E`lásko `F`má, vem mě `Dm`s sebou!
`C`Ne, to `F`nesmí `C`být!

1. Můj šál skryje proud vlasů, na pás pak připnu nůž,
poznáš jen podle hlasu, že já nejsem muž,
tvůj kapitán tě čeká, pojď, musíme už jít,
noc už svůj kabát svléká…
Ne, to nesmí být!

1. Až dým vítr stočí, tvář změní pot a prach,
do mých dívej se očí, tam není strach,
když výstřel tě raní, kdo dával by ti pít,
hlavu vzal do svých dlaní…
Ne, to nesmí být!

1. Ach, má lásko sladká, jak mám ti to jen říct,
každá chvíle je krátká a já nemám víc,
já mám jenom tebe, můj dech jenom tvůj zná,
nech mě jít vedle sebe…
Pojď, lásko má!
