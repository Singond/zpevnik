# Mezi horami
## Čechomor

1. [: `Am`Mezi `G`hora`Am`mi, lipka `G`zele`Am`ná, :]
[: `C`zabili Janka,  `G`Janíčka, Ja`Am`nka, miesto `G`jele`Am`ňa. :]

2. [: Keď ho zabili, zamordovali, :]
[: na jeho hrobě, na jeho hrobě, kříž postavili. :]

3. [: Ej křížu, křížu, ukřižovaný, :]
[: zde leží Janík, Janíček, Janík, zamordovaný. :]

4. [: Tu šla Anička, plakat Janíčka, :]
[: hned na hrob padla a viac nevstala, dobrá Anička. :]
