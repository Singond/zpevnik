# Pocestný
## Spirituál kvintet

1. `G`Je to chůze `G7`po tom světě, `C`kam se noha `G`šine,
[: `D`sotva přej`H7`deš `Em`jedny ho`C`ry, `G`hned se naj`D`dou `G`jiné. :]

2. Je to chůze po tom světě, že by člověk utek',
[: ještě nezažil jsi jeden, máš tu druhý smutek. :]

3. Což je pánům v krytém voze, sedí pěkně v suše,
[: ale chudý, ten za nimi v dešti, v blátě kluše. :]

4. Ej, co já dbám na své pouti na psoty a sloty,
[: jen když já mám zdravé nohy, k tomu dobré boty. :]

5. Však na pány v krytém voze taky někdy trhne,
[: jednou se jim kolo zláme, jindy vůz se zvrhne. :]

6. A krom toho, až své pouti přejedem a přejdem,
[: v jedné hospodě na nocleh pán-nepán se sejdem. :]