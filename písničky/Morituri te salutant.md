# Morituri te salutant
## Karel Kryl

1. Cesta je `Am`prach a `G`šterk a `Dm`udusaná `Am`hlína
`C`a šedé `F`šmouhy `G7`kreslí do vla`C`sů
a z hvězdných `Dm`drah má `G`šperk co `C`kamením se `E`spíná
`Am`a pírka `G`touhy z `Em`křídel pega`Am`sů.
A z hvězdných `F`drah má `G`šperk co `C`kamením se `E`spíná
`Am`a pírka `G`touhy z `Em`křídel pega`Am`sů.

2. Cesta je bič, je zlá jak pouliční dáma,
má v ruce štítky, v pase staniol,
[: a z očí chtíč jí plá, když háže do neznáma,
dvě křehké snítky rudých gladiol. :]

> `G`Seržante písek je bílý jak paže Daniely,
`Am`počkejte chvíli mé oči uviděly
`G`tu strašne dávnou vteřinu zapomnění,
`Am`seržante, mávnou `G7`a budem zasvěceni.
`Am`Morituri te salutant, morituri te `G`salutant…

3. Tou cestou dál jsem šel, kde na zemi se zmítá
a písek víří křídla holubí
[: a marš mi hrál zvuk děl co uklidnění skýtá,
a zvedá chmýři které zahubí. :]

4. Cesta je tér a prach a udusaná hlína,
mosazná včelka od vlkodlaka,
[: rezavý kvér, můj brach a sto let stará špína
a děsně velká bíla oblaka. :]

> Seržante…
