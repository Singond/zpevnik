{{!--
 The content of all variables is by default escaped with a TeX
 escaping function similar to how special characters are escaped in HTML.
 The escaping function doesn't escape spaces.
 To preserve spaces, use {{{ (pre variable) }}}, which replaces spaces
 with the non-breaking space TeX entity '~'.
--}}

{{!-- Document header --}}

\documentclass[11pt,a5paper,openany]{memoir}
\raggedbottom

\usepackage{geometry}
\geometry{
  a5paper,
  top=10mm,
  left=18mm,
  right=21mm,
  bottom=18mm,
%  showframe,
}
\usepackage{fontspec}
\usepackage{titlesec}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage[defaultlines=5,all]{nowidow}
\usepackage[pdfusetitle,colorlinks=true]{hyperref}
\usepackage{float}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{marginnote}
\usepackage{amssymb}

%% Page style
\makepagestyle{songs}
% Hack: Right pagination is moved right by using the margin
\makeoddfoot{songs}{}{}{\marginnote{\hspace{2mm}$\langle$\thepage$\rangle$}}
% Hack: Left pagination is moved left with a kern
\makeevenfoot{songs}{\kern-8mm$\langle$\thepage$\rangle$}{}{}
% Patch cleardoublepage to not get blank pages after title & contents pages:
\renewcommand\cleardoublepage{\clearpage}
% Suppress page numbers in the toc/frontmatter:
\aliaspagestyle{chapter}{empty}

%% Fonts and colours
\setmainfont[Ligatures=TeX]{Droid Serif}
\setsansfont[Ligatures=TeX]{Droid Sans}
\colorlet{LightRed}{red!65!}
\colorlet{DarkGray}{black!70!}

%% Spacings
\setlength{\parindent}{0pt}
\setlength{\tabcolsep}{0pt}
\setlength{\parskip}{1mm}

%% ToC style
% Hide the title of the ToC:
\renewcommand\tocheadstart{}
\renewcommand\printtoctitle[1]{}
\renewcommand\aftertoctitle{}
% Hide section numbers in the ToC:
\renewcommand\numberline[1]{}
\renewcommand\cftdotsep{1}

%% Hyperlinks setup
\hypersetup{
  bookmarks=true,
  linkcolor=.,
  urlcolor=blue,
  pdfcreator={{ program.name }} v. {{ program.version }} - {{ program.homepage }},
}

%% Song title and subtitle formats
\titleformat{\section}
  {\large\bfseries}{}{0pt}{\underline}
\titlespacing*{\section}
  {0pt}{7mm}{0pt}
\newcommand\songtitle[1]{%
  % This is a trick to only layout a song on the current page
  % if it fits, otherwise a pagebreak is inserted
  \FloatBlock
  \vfil
  \pagebreak[2]
  \vfilneg
  \section{#1}
}
\newcommand\subtitle[1]{%
  \emph{#1}
}

%% Verse layout command
\makeatletter
% The verse & label layout code was written by Jonathan P. Spratte
% under the Beerware license: As long as you retain this notice you
% can do whatever you want with this stuff. If we meet some day, and you think
% this stuff is worth it, you can buy me a beer in return. Jonathan P. Spratte
\newlength\verse@indent
\newlength\verse@labelsep
\newlength\verse@vskip
\AtBeginDocument{% setting AtBeginDocument since earlier we can't rely on em being correct
  \verse@indent=8mm
  \verse@labelsep=1mm
  \verse@vskip=\smallskipamount
}
\newcommand\Verse[1]{%
    \par
    \vskip\verse@vskip
    \noindent\kern-\verse@indent
    \sbox0{\textbf{\footnotesize{#1}}}%
    \ifdim\wd0>\dimexpr\verse@indent-\verse@labelsep\relax
      \usebox0\kern\verse@labelsep
    \else
      \makebox[\verse@indent][l]{\usebox0}%
    \fi
    \ignorespaces
}

%% Custom date format
\newcommand\numdate[0]{\number\day.\number\month.\number\year}

\makeatother

{{!-- HB inlines: Block types --}}

{{#*inline "verse-label"}}
  {{~#if verse}}{{verse}}.{{/if~}}
  {{~#if (contains this "chorus")}}{{@root.book.chorus_label}}{{chorus}}.{{/if~}}
  {{~#if custom}}{{custom}}{{/if~}}
{{/inline}}

{{#*inline "b-verse"~}}
  {{#each paragraphs~}}
    {{#if @first}}\Verse{ {{~>verse-label ../label ~}} }{{/if}} {{#each this}}{{> (lookup this "type") }}{{/each}}

    \vspace{\parskip}

  {{/each}}
{{/inline}}

{{#*inline "b-bullet-list"~}}
  \begin{itemize}[noitemsep]{{#each items}}\item {{ this }}
{{/each}}
  \end{itemize}
{{/inline}}

{{#*inline "b-horizontal-line"}}
  \vphantom{}\hrule
{{/inline}}

{{#*inline "b-pre"}}
  \begin{verbatim}{{{ text }}}\end{verbatim}
{{/inline}}

{{!-- HB inlines: Inline types --}}

{{#*inline "i-text"}}{{{(pre text)}}}{{/inline}}

{{#*inline "chord-style"~}}
  {{!-- Helper for i-chord for setting styles based on ` vs `` chords --}}
  {{~#if (eq backticks 1) }}\textbf{\sffamily\color{red}{{/if~}}{{~#unless (eq backticks 1) }}\small{\sffamily\color{LightRed}{{/unless~}}
{{~/inline~}}
{{#*inline "i-chord"~}}
  \begin{tabular}[b]{l}
    {{> chord-style}}{ {{{~ (pre chord) ~}}} }}\\
    {{~#if alt_chord}}{{> chord-style}}\color{blue}{ {{{~ (pre alt_chord) ~}}} }}\\{{/if~}}
    {{~#each inlines}}{{> (lookup this "type") }}{{/each~}}\mbox{}\end{tabular}
{{~/inline}}

{{!-- Nb. the i-break element is a line separator, not terminator,
  ie. no i-break after the last inline element. --}}
{{#*inline "i-break"}}\\
{{/inline}}
{{#*inline "i-emph"}}\emph{ {{~#each inlines}}{{> (lookup this "type") }}{{/each~}} }{{/inline}}
{{#*inline "i-strong"}}\textbf{ {{~#each inlines}}{{> (lookup this "type") }}{{/each~}} }{{/inline}}
{{#*inline "i-link"}}\href{ {{~ url ~}} }{ {{{~ (pre text) ~}}} }{{/inline}}
{{#*inline "i-chorus-ref"}}{{ prefix_space }}\textbf{ {{~ @root.book.chorus_label }}{{ num }}.}{{/inline}}

{{#*inline "i-image"}}
  {{~#if (eq class "center") }}

    \begin{figure}[H]
      \centering
      \includegraphics[width={{ px2mm (img_w path) }}mm]{ {{~ path ~}} }
    \end{figure}

  {{/if~}}
  {{~#if (eq class "right") }}
    \hfill\hspace{0pt}\vspace{-1em}
    {
    \begin{wrapfigure}{r}{ {{~ px2mm (img_w path) }}mm}
      \centering
      \includegraphics[width={{ px2mm (img_w path) }}mm]{ {{~ path ~}} }
    \end{wrapfigure}
    }
  {{/if~}}
  {{~#unless class }}\includegraphics[width={{ px2mm (img_w path) }}mm]{ {{~ path ~}} }{{/unless~}}
{{/inline}}

{{!-- Main content --}}

% Metadata
\title{ {{~ book.title ~}} }

% Document
\begin{document}

%% Title page
\frontmatter*
\newgeometry{margin=5mm}
\begin{titlingpage*}
  \begin{vplace}[0.5]
    \begin{center}
      % \Huge{\textbf{ {{~ book.title ~}} }} \\
      % \vspace{0.5cm}
      {{~#if book.front_img}}
        \includegraphics[width={{ px2mm (img_w book.front_img) }}mm]{ {{~ book.front_img ~}} }
      {{/if}}
      \vspace{4mm}
      \huge{ {{~ book.subtitle ~}} }\\
      \vspace{1cm}
      \includegraphics[width={{ px2mm (img_w book.front_img2) }}mm]{ {{~ book.front_img2 ~}} }
    \end{center}
  \end{vplace}

  \mbox{}
  \vfill
  \begin{center}\small{ {{~ book.title_note ~}} }\end{center}
\end{titlingpage*}
\restoregeometry

%% Contents page
\renewcommand*{\contentsname}{}
\pagenumbering{gobble}
\tableofcontents*

%% Songs
\mainmatter*

\pagestyle{songs}
\pagenumbering{arabic}
\setcounter{page}{4}
{{#each songs -}}
  %% song {{ @index }}
  \songtitle{ {{~ title ~}} }

  {{#if subtitles ~}}
    {{#each subtitles}}\subtitle{ {{~ this ~}} }{{#unless @last}}\\\{{/unless}}{{/each}}
    \vspace{2mm}
  {{/if}}
  {{#unless subtitles}}\vspace{2mm}{}{{/unless}}

  {{!-- Dispatch to block HB inlines prefixed b- , see above --}}
  {{#each blocks}}{{> (lookup this "type") }}{{/each}}
{{/each}}

\backmatter
\pagestyle{empty}
\clearpage
\mbox{}
\clearpage
\vspace*{\fill}
\begin{center}
\footnotesize{\color{DarkGray}Tvrz -- zpěvník. Vysázeno \numdate.}
\end{center}

\end{document}
